###########################################################################
# BillowConf is Copyright (C) 2020 Kyle Robbertze <kyle@debian.org>
#
# BillowConf is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# BillowConf is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BillowConf. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import os
from logging.handlers import RotatingFileHandler
from django.conf import settings

class BillowConfFileHandler(RotatingFileHandler):
    
    def __init__(self, prod_filename, dev_filename, **kwargs):
        access = os.access(prod_filename, os.W_OK)
        if access:
            kwargs['filename'] = prod_filename
        else:
            kwargs['filename'] = os.path.join(os.getcwd(), dev_filename)
        super().__init__(**kwargs)
