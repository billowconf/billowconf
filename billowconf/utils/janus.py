###########################################################################
# BillowConf is Copyright (C) 2020 Kyle Robbertze <kyle@debian.org>
# File adapted from aiortc (https://github.com/aiortc/aiortc) Janus
# example copyright (C) 2019 Jeremy Lainé <jeremy.laine@m4x.org>
#
# BillowConf is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# BillowConf is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BillowConf. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import aiohttp
import asyncio
import datetime
import random
import signal
import string
import time
import logging
from aiortc import RTCPeerConnection, RTCSessionDescription, VideoStreamTrack, RTCConfiguration, RTCIceServer
from aiortc.contrib.media import MediaPlayer
from asgiref.sync import sync_to_async
from django.conf import settings
from django.utils import timezone
from billowconf.models import Room

logger = logging.getLogger(__name__)

IGNORED_ROOMS = ['Elsewhere™','Somewhere™']
IGNORED_ROOMS = [x.lower() for x in IGNORED_ROOMS]
pcs = set()

def transaction():
    return ''.join(random.choices(string.ascii_lowercase + string.digits, k=7))

class JanusError(RuntimeError):
    pass

class JanusRoomDoesNotExistError(JanusError):
    pass

class JanusPlugin:
    def __init__(self, session, url):
        self.queue = asyncio.Queue()
        self.session = session
        self.url = url

    async def send(self, payload):
        message = {'janus': 'message', 'transaction': transaction()}
        message.update(payload)
        logger.info('Sending message')
        logger.debug(message)
        async with self.session.http.post(self.url, json=message) as response:
            response = await response.json()
            if response['janus'] == 'ack':
                response = await self.queue.get()
            return response

class JanusSession:
    def __init__(self, url):
        self.http = None
        self.poll_task = None
        self.plugins = {}
        self.root_url = url
        self.session_url = ''

    async def attach(self, plugin_name):
        message = {
            'janus': 'attach',
            'plugin': plugin_name,
            'transaction': transaction(),
        }

        async with self.http.post(self.session_url, json=message) as response:
            data = await response.json()
            plugin_id = data["data"]["id"]
            plugin = JanusPlugin(self, f'{self.session_url}/{plugin_id}')
            self.plugins[plugin_id] = plugin
            return plugin

    async def create(self):
        self.http = aiohttp.ClientSession()
        message = {
            'janus': 'create',
            'transaction': transaction(),
        }
        async with self.http.post(self.root_url, json=message) as response:
            data = await response.json()
            session_id = data["data"]["id"]
            logger.info(f'Created session: {session_id}')

            self.session_url = f'{self.root_url}/{session_id}'
            self.poll_task = asyncio.ensure_future(self.poll())

    async def destroy(self):
        if self.poll_task:
            self.poll_task.cancel()
            self.poll_task = None

        if self.session_url:
            message = {
                'janus': 'destroy',
                'transaction': transaction(),
            }
            await self.http.post(self.session_url, json=message)
            self.session_url = ''

    async def poll(self):
        logger.info('Polling Janus...')
        while True:
            params = {
                'maxev': 1,
                'rid': int(time.time() * 1000),
            }
            async with self.http.get(self.session_url, params=params) as response:
                data = await response.json()
                if data["janus"] == "event":
                    plugin = self.plugins.get(data["sender"], None)
                    if plugin:
                        await plugin.queue.put(data)
                    else:
                        print(data)

async def publish(plugin, player, display='placeholder'):
    ice_servers = []
    for server in settings.JANUS['ICE_SERVERS']:
        ice_servers.append(RTCIceServer(server['urls']))
    configuration = RTCConfiguration(ice_servers)
    pc = RTCPeerConnection(configuration=configuration)
    pcs.add(pc)
    media = {
        'audio': False,
        'video': True,
    }
    if player and player.audio:
        pc.addTrack(player.audio)
        media['audio'] = True

    if player and player.video:
        pc.addTrack(player.video)
    else:
        pc.addTrack(VideoStreamTrack())

    await pc.setLocalDescription(await pc.createOffer())
    request = {
        'request': 'publish',
        'display': display,
    }
    request.update(media)
    response = await plugin.send({
        'body': request,
        'jsep': {
            'sdp': pc.localDescription.sdp,
            'trickle': False,
            'type': pc.localDescription.type,
        },
    })
    if 'jsep' not in response:
        raise JanusError(f'JSEP expected, but not returned: {response}')
    await pc.setRemoteDescription(RTCSessionDescription(sdp=response['jsep']['sdp'],
                                                        type=response['jsep']['type']))
    logger.info(f'Publishing stream: {pc}')
    return pc

async def subscribe(session, room, feed, recorder):
    pc = RTCPeerConnection()
    pcs.add(pc)

    @pc.on('track')
    async def on_track(track):
        if track.kind == 'video':
            recorder.addTrack(track)
        if track.kind == 'audio':
            recorder.addTrack(track)

    plugin = await session.attach('janus.plugin.videoroom')
    response = await plugin.send({
        'body': {
            'request': 'join',
            'ptype': 'subscriber',
            'room': room,
            'feed': feed,
        }
    })

    if not 'jsep' in response:
        print(response)

    await pc.setRemoteDescription(RTCSessionDescription(sdp=response['jsep']['sdp'],
                                                        type=response['jsep']['type']))

    await pc.setLocalDescription(await pc.createAnswer())
    response = await plugin.send({
        'body': { 'request': 'start'},
        'jsep': {
            'sdp': pc.localDescription.sdp,
            'trickle': False,
            'type': pc.localDescription.type,
        },
    })
    await recorder.start()
    return pc

async def connect(webrtc_channel, display, session):
    await session.create()
    plugin = await session.attach('janus.plugin.videoroom')
    response = await plugin.send({
        'body': {
            'display': display,
            'ptype': 'publisher',
            'request': 'join',
            'room': webrtc_channel,
        },
    })
    plugin_data = response['plugindata']['data']
    if 'error_code' in plugin_data:
        if plugin_data['error_code'] == 426 or plugin_data['error_code'] == 430:
            # Room doesn't exist, and needs re-creating
            raise JanusRoomDoesNotExistError(plugin_data)
    publishers = plugin_data['publishers']
    for publisher in publishers:
        if publisher['display'] == 'server':
            return None
    return plugin

async def run(player, webrtc_channel, talk_set, session):
    plugin = await connect(webrtc_channel, 'placeholder', session)
    if plugin is None:
        logger.info('Another thread already publishing. Nothing to do.')
        return
    publish_pc = await publish(plugin=plugin,
                               player=player,
                               display='placeholder')
    sender = publish_pc.getSenders()[0]

    @sender.transport.on('statechange')
    async def change_state():
        # Required for using the surrounding function's sender
        nonlocal sender
        logger.info(f'New state: {sender.transport.state}')
        if sender.transport.state == 'closed':
            nonlocal plugin, player, publish_pc
            await plugin.send({'body': {'request': 'unpublish'}})
            await publish_pc.close()
            try:
                publish_pc = await publish(plugin=plugin,
                                           player=player,
                                           display='placeholder')
            except JanusError as e:
                plugin = await connect(webrtc_channel, 'placeholder', session)
                publish_pc = await publish(plugin=plugin,
                                           player=player,
                                           display='placeholder')
            sender = publish_pc.getSenders()[0]

    while True:
        start = timezone.now()
        end = start + datetime.timedelta(seconds=60)
        talks = talk_set.filter(start__range=(start, end)).exclude(recorded_content=None)
        exists = await sync_to_async(talks.exists, thread_sensitive=True)()
        if exists:
            talk = await sync_to_async(talks.first, thread_sensitive=True)()
            player = MediaPlayer(talk.recorded_content)
            await plugin.send({'body': {'request': 'unpublish'}})
            await publish_pc.close()
            try:
                publish_pc = await publish(plugin=plugin,
                                           player=player,
                                           display='recorded-presenter')
            except JanusError as e:
                plugin = await connect(webrtc_channel, 'recorded-presenter', session)
                publish_pc = await publish(plugin=plugin,
                                           player=player,
                                           display='recorded-presenter')
            sleep = talk.start + talk.duration - timezone.now()
            await asyncio.sleep(sleep.seconds)
            await plugin.send({'body': {'request': 'unpublish'}})
            await publish_pc.close()
            player = None
            try:
                publish_pc = await publish(plugin=plugin,
                                           player=player,
                                           display='placeholder')
            except JanusError as e:
                plugin = await connect(webrtc_channel, 'placeholder', session)
                publish_pc = await publish(plugin=plugin,
                                           player=player,
                                           display='placeholder')
        else:
            await asyncio.sleep(60)

async def create_room(room_name, session):
    await session.create()
    plugin = await session.attach('janus.plugin.videoroom')
    response = await plugin.send({
        'body': {
            'request': 'create',
            'description': room_name,
            'room': random.randint(0, 999999),
            'is_private': False,
        }
    })
    await session.destroy()
    return response

def create_janus_room(room_name):
    url = f'https://{settings.JANUS["SERVER"]}{settings.JANUS["ROOT"]}'
    session = JanusSession(url)
    loop = asyncio.get_event_loop()
    response = None
    try:
        response = loop.run_until_complete(create_room(room_name,
                                                       session=session))
    except KeyboardInterrupt:
        pass
    finally:
        stop_event_loop(session)

    if response:
        room_id = response['plugindata']['data']['room']
        return room_id
    return -1

def stream_janus_room(room):
    url = f'https://{settings.JANUS["SERVER"]}{settings.JANUS["ROOT"]}'
    session = JanusSession(url)
    loop = asyncio.get_event_loop()
    media = room.conference.placeholder
    player = None
    if media:
        logger.info(f'Media available: {media}')
        # TODO: loop file indefinitely. These options do not work
        player = MediaPlayer(media, options={'-filter_complex': 'loop=-1:25:1'})
    try:
        loop.run_until_complete(run(player=player,
                                    webrtc_channel=room.webrtc_channel,
                                    talk_set=room.talk_set,
                                    session=session))
    except KeyboardInterrupt:
        pass
    except JanusRoomDoesNotExistError:
        response = loop.run_until_complete(create_room(room_name, session=session))
        room_id = response['plugin_data']['data']['room']
        room.webrtc_channel = room_id
        room.save()
        try:
            loop.run_until_complete(run(player=player,
                                        webrtc_channel=room.webrtc_channel,
                                        talk_set=room.talk_set,
                                        session=session))
        except KeyboardInterrupt:
            pass
    finally:
        stop_event_loop(session)

def stop_event_loop(session):
    loop = asyncio.get_event_loop()
    loop.run_until_complete(session.destroy())
    coros = [pc.close() for pc in pcs]
    loop.run_until_complete(asyncio.gather(*coros))
