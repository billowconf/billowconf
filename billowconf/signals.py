###########################################################################
# BillowConf is Copyright (C) 2020 Kyle Robbertze <kyle@debian.org>
#
# BillowConf is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# BillowConf is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BillowConf. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import logging
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
from .models import Room
from .tasks import create_room

logger = logging.getLogger(__name__)

@receiver(post_save, sender=Room, dispatch_uid='signal_create_webrtc_room')
def create_webrtc_room(sender, instance=None, **kwargs):
    if instance.webrtc_channel == None:
        logger.info(f'Creating WebRTC room for room {instance.name}')
        create_room.delay(instance.id)

@receiver(post_save, sender=settings.AUTH_USER_MODEL, dispatch_uid='signal_create_auth_token')
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        logger.info(f'Creating token for user {instance.username}')
        Token.objects.create(user=instance)
