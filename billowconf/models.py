###########################################################################
# BillowConf is Copyright (C) 2020 Kyle Robbertze <kyle@debian.org>
#
# BillowConf is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# BillowConf is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BillowConf. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import random
import os
import binascii
import datetime
import logging
from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

logger = logging.getLogger(__name__)

def conference_path(instance, filename):
    return f'conferences/{instance.id}/{filename}'

def talk_path(instance, filename):
    return f'conferences/{instance.room.conference.id}/talks/{instance.id}_{filename}'

class Conference(models.Model):
    name = models.CharField(max_length=255)
    irc_server = models.CharField(max_length=255, default='')
    start_date = models.DateField()
    end_date = models.DateField()
    placeholder = models.FileField(upload_to=conference_path, blank=True, null=True)

    def __str__(self):
        return self.name

class Room(models.Model):
    name = models.CharField(max_length=255)
    conference = models.ForeignKey(Conference, on_delete=models.CASCADE)
    irc_channel = models.CharField(max_length=200, default='')
    webrtc_channel = models.IntegerField(null=True, blank=True)
    active_webrtc_feed = models.IntegerField(null=True, blank=True)

    @property
    def stream(self):
        return self.pk

    def __str__(self):
        return self.name

class Talk(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    start = models.DateTimeField()
    duration = models.DurationField()
    presenters = models.ManyToManyField(settings.AUTH_USER_MODEL)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    recorded_content = models.FileField(upload_to=talk_path, blank=True, null=True)

    def __str__(self):
        return f"{self.name} ({self.presenters})"

class ConferenceManagementPerson(models.Model):
    management_id = models.IntegerField()
    conference = models.ForeignKey(Conference, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.user}'

class ResetPasswordToken(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE,
                             related_name='password_reset_tokens')
    created_at = models.DateTimeField(auto_now_add=True)
    key = models.CharField(max_length=64, unique=True)

    @staticmethod
    def generate_key():
        logger.info('Generating password reset key')
        length = random.randint(10, 50)
        return binascii.hexlify(os.urandom(50)).decode()[0:length]

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super().save(*args, **kwargs)

def create_reset_token(email_serializer):
    logger.debug('Running create_reset_token')
    email_serializer.is_valid(raise_exception=True)
    email = email_serializer.validated_data['email']

    expired_limit = timezone.now() - datetime.timedelta(hours=24)
    ResetPasswordToken.objects.filter(created_at__lte=expired_limit).delete()
    users = User.objects.filter(username__iexact=email)

    user = None
    if users.exists():
        user = users.get()

    if user is None or not user.is_active:
        return None

    logger.info(f'Creating reset key for user {user.get_full_name}')
    token = None
    if user.password_reset_tokens.all().exists():
        token = user.password_reset_tokens.all().first()
    else:
        token = ResetPasswordToken.objects.create(user=user)
    return token
