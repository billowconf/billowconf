###########################################################################
# BillowConf is Copyright (C) 2020 Kyle Robbertze <kyle@debian.org>
#
# BillowConf is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# BillowConf is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BillowConf. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
from django_filters import rest_framework as filters
from django.db.models import Q
from .models import Talk

class TalkFilter(filters.FilterSet):
    has_recorded_content = filters.BooleanFilter(field_name='recorded_content',
                                                 method='filter_recorded_content')

    def filter_recorded_content(self, queryset, name, value):
        fltr = Q(**{name: ''}) | Q(**{name: None})
        if value:
            fltr = ~fltr
        return queryset.filter(fltr)

    class Meta:
        model = Talk
        fields = ['name', 'room', 'has_recorded_content']
